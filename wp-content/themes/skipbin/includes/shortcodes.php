<?php
function socmed_agogo($atts, $content){
	global $redux;
	ob_start();
?>
	<ul class="menu">
		<?php if (!empty($redux['facebook-link'])):?>
			<li><a href="<?php echo $redux['facebook-link'];?>"><i class="fa fa-facebook"></i>Facebook</a></li>
		<?php endif;?>
		<?php if (!empty($redux['instagram-link'])):?>
			<li><a href="<?php echo $redux['instagram-link'];?>"><i class="fa fa-instagram"></i>Instagram</a></li>
		<?php endif;?>
		<?php if (!empty($redux['skype-link'])):?>
			<li><a href="<?php echo $redux['skype-link'];?>"><i class="fa fa-skype"></i>Skype</a></li>
		<?php endif;?>
		
		<?php if (!empty($redux['google-plus-link'])):?>
			<li><a href="#"><i class="fa fa-google-plus"></i>Google Plus</a></li>
		<?php endif;?>
	</ul>
<?php
	return ob_get_clean();
}
add_shortcode( 'ezyskipbin_socmed_agogo', 'socmed_agogo' );


function enquire_form_popup($atts){
	global $redux;

	ob_start();
?>
	<div class="modal fade" id="enquire-form-home" tabindex="-1" role="dialog" aria-labelledby="enquire_form">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><i class="fa fa-envelope-o" aria-hidden="true"></i>Enquire Form</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					
				</div>
				<div class="modal-body">
					<?php echo do_shortcode( '[contact-form-7 id="'.$redux['enquire-form'].'"]' ); ?>
				</div>
			</div>
		</div>
	</div>

<?php
	return ob_get_clean();
}
add_shortcode('ezyskipbin_form_popup', 'enquire_form_popup');

function accordion_wrap($atts, $content){
	ob_start();
?>
	<div class="ezyskip-accordion">
		<?php echo do_shortcode( $content ); ?>
	</div>

	<script>
		jQuery(document).ready(function($){
			$('.ezyskip-accordion').accordion({
				collapsible: true,
				active: false, 
				heightStyle: "content",
				header: "> div > h3"
			});
		})
	</script>
<?php
	return ob_get_clean();
}
add_shortcode( 'ezyskipbin_accordion', 'accordion_wrap' );

function accordion_title($atts, $content){
	return '<div class="accordion-item"><h3 class="has-content">'.$content.'<i class="fa fa-angle-down float-right"></i></h3>';
}
add_shortcode( 'ezyskipbin_accordion_title', 'accordion_title' );
function accordion_title_no_content($atts, $content){
	return '<div class="accordion-item"><h3 class="no-content">'.$content.'</h3>';
}
add_shortcode( 'ezyskipbin_accordion_title_no_content', 'accordion_title_no_content' );
function accordion_content($atts, $content){
	return '<div>'.$content.'</div></div>';
}
add_shortcode( 'ezyskipbin_accordion_content', 'accordion_content' );
function accordion_content_no_content($atts, $content){
	return '</div>';
}
add_shortcode( 'ezyskipbin_accordion_content_no_content', 'accordion_content_no_content' );

function size_list($atts, $content){
	$args = array(
		'post_type' => 'bin_size', 
		'order'		=> 'ASC',
	);
	$query = new WP_Query( $args );
	ob_start();
?>
	<?php if ( $query->have_posts()) : ?>
		<div style="clear:both;"></div>
		<div class="row align-items-center">
			<?php while( $query->have_posts()) : $query->the_post();?>
				<div class="col-12 col-lg-6">
					<div class="size-list p-3 mb-3">
						<div class="row align-items-center">
							<div class="col-12">
								<?php 
										$thumb_url = get_the_post_thumbnail_url($post->ID); 
										$image_alt = get_post_meta( $thumb_url, '_wp_attachment_image_alt', true);?>
										<img class="img-responsive" src="<?php the_post_thumbnail_url($post->ID)?>" alt="<?php echo $image_alt;?>" />
							</div>
							<div class="col-12">
								<div class="bin-content">
									<?php the_content();?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile;?>
		</div>
	<?php endif;?>
	<?php wp_reset_postdata(); ?>
	<?php return ob_get_clean();?>
<?php
}
add_shortcode( 'ezyskipbin_binsize', 'size_list');
?>