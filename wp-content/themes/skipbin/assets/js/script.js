(function($){
	$(document).ready(function(){
		
		$(document).find('[name="compare"]').val('');
		/*PRELOADER JS*/
			setTimeout(function(){
				$('body').addClass('loaded');
				$('h1').css('color','#e8e3dd');
			}, 2000);
		/*END PRELOADER JS*/
		$('[data-toggle="popover"]').popover();
		$('[name="deliverydate"]').daterangepicker({
			singleDatePicker: true,
			startDate: moment().add(1, 'days'),
			minDate: moment().add(1, 'days'),
			locale: {
				format: 'DD/MM/YYYY'
			}
			
		});
		
		$('[name="collectiondate"]').daterangepicker({
			singleDatePicker: true,
			startDate: moment().add(2, 'days'),
			minDate: moment().add(2, 'days'),
			locale: {
				format: 'DD/MM/YYYY'
			}
		});
		
		function goToByScroll(id){
			// Remove "link" from the ID
			id = id.replace("link", "");
			var targetOffset = $(id).offset().top - 7;
			// Scroll
			$('html,body').animate({
				scrollTop: targetOffset},
				'slow');
		}
		$('.tabs')
			.tabs({active: 0})
			.addClass('ui-tabs-vertical ui-helper-clearfix');
		$('.carousel').carousel({
			interval:6000
		});
		$('.differences-carousel').owlCarousel({
			items:1,
			itemsDesktop:[1000,1],
			itemsDesktopSmall:[979,2],
			itemsTablet:[768,1],
			pagination:true,
			navigation:true,
			autoplay:true
		});
		
		$('.bin-size-slider').slick({
			arrows: true,
			infinite: false,
			speed: 300,
			slidesToShow: 4,
			slidesToScroll: 4,
			responsive: [
				{
				breakpoint: 1024,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
						dots: true
					}
				},
				{
				breakpoint: 600,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2,
						dots:true,
						arrows: false,
						centerMode: false,
					}
				},
				{
				breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots:true,
						arrows: false,
						centerMode: true,
					}
				}
			]
		});
		//ajax api
		$('#area-placeholder').hide();
		$('#postcode-placeholder').hide();
		$('#area-loading').css('visibility', 'hidden');
		$('#modalState').on('change',function(){
			$('#area-placeholder').hide();
			$('#postcode-placeholder').hide();
			$('#area-loading').css('visibility', 'visible');
			var parent = $(this).parents('#state-placeholder');
			var parent_sibling = parent.siblings('#area-placeholder')
			var zipcode = $(this).val();
			$.ajax({
				type: "POST",
				url: theme_param.ajaxurl,
				//dataType: 'json',
				data: {
					action : 'fetch_area',
					zipcode : zipcode,
				},
				 success: function(data){
					var area = parent_sibling.find('#modalArea');
					area.html();
					area.html('<option value="#" id="selected">Select Region .. </option>')
					area.append(data);
				},
				error: function( data, status, error ) { 
					console.log(data);
					console.log(status);
					console.log(error);
				}
			})
			.done(function( data ) {
				$('#area-placeholder').show();
				$('#area-loading').css('visibility', 'hidden');
				console.log('Successful AJAX Call! /// Return Data: ' + data);
			})
			.fail(function( data ) {
				console.log('Failed AJAX Call :( /// Return Data: ' + data);
			});
		});
		
		
		$('#postcode-placeholder').hide();
		$('#area-loading').css('visibility', 'hidden');
		$('#modalArea').on('change',function(){
			$('#postcode-placeholder').hide();
			$('#postcode-loading').css('visibility', 'visible');
			var parent = $(this).parents('#area-placeholder');
			var parent_sibling = parent.siblings('#postcode-placeholder')
			var zipcode = $(this).val();
			$.ajax({
				type: "POST",
				url: theme_param.ajaxurl,
				//dataType: 'json',
				data: {
					action : 'fetch_postcode',
					zipcode : zipcode,
				},
				 success: function(data){
					var area = parent_sibling.find('#modalPostcode');
					area.html();
					area.html('<option value="#" id="selected">Select Region .. </option>')
					area.append(data);
				},
				error: function( data, status, error ) { 
					console.log(data);
					console.log(status);
					console.log(error);
				}
			})
			.done(function( data ) {
				$('#postcode-placeholder').show();
				$('#postcode-loading').css('visibility', 'hidden');
				console.log('Successful AJAX Call! /// Return Data: ' + data);
			})
			.fail(function( data ) {
				console.log('Failed AJAX Call :( /// Return Data: ' + data);
			});
		});
		
		$('#modalPostcode').on('change',function(){
			var finalzipcode = $(this).val();
			$(document).find('[name="compare"]').val(finalzipcode);
			$('#postcode_search').modal('hide');
		});
		
		$('.bin-hire-check').on('click',function(){
			var val = $(this).parent().find('[name="val"]').val();
			var zipcode = $(this).parent().find('[name="zipcode"]').val();
			console.log(zipcode);
			console.log(val);
			$.ajax({
				type: "POST",
				url: theme_param.ajaxurl,
				data: {
					action : 'a',
					bintype : val,
					zipcode : zipcode,
				},
				 success: function(data){
					console.log(data);
				},
				error: function( data, status, error ) { 
					console.log(data);
					console.log(status);
					console.log(error);
				}
			})
			.done(function( data ) {
				console.log('Successful AJAX Call! /// Return Data: ' + data);
				window.location.replace(data);
			})
			.fail(function( data ) {
				console.log('Failed AJAX Call :( /// Return Data: ' + data);
			});
		});
		
		$('.form-check').hover(function(){
			var sizedesc = $(this).find('.size-description').val();
			$('.size-detail').html(sizedesc);
		}, function(){ // hover out
			$('.size-detail').html('');
		});
		
		$('.bin-size-check').on('click',function(){
			var val = $(this).val();
			var zipcode = $(this).parent().find('[name="zipcode"]').val();
			var bintype = $(this).parent().find('[name="bintype"]').val();
			console.log(val);
			$.ajax({
				type: "POST",
				url: theme_param.ajaxurl,
				data: {
					action : 'storesize',
					bintype : bintype,
					zipcode : zipcode,
					size: val,
				},
				 success: function(data){
					console.log(data);
				},
				error: function( data, status, error ) { 
					console.log(data);
					console.log(status);
					console.log(error);
				}
			})
			.done(function( data ) {
				console.log('Successful AJAX Call! /// Return Data: ' + data);
				window.location.replace(data);
			})
			.fail(function( data ) {
				console.log('Failed AJAX Call :( /// Return Data: ' + data);
			});
		});
		
		$('#submitdate_binhire').on('click', function(){
			var deliverydate = $('[name="deliverydate"]').val();
			var collectiondate = $('[name="collectiondate"]').val();
			var bintype = $('[name="bintype"]').val();
			var zipcode = $('[name="zipcode"]').val();
			var size = $('[name="size"]').val();
			
			var deliv_date = deliverydate.substring(0,2);
			var deliv_month = deliverydate.substring(3,5);
			var deliv_year = deliverydate. substring(6,10);
			
			var col_date = collectiondate.substring(0,2);
			var col_month = collectiondate.substring(3,5);
			var col_year = collectiondate. substring(6,10);
			
			var delivdate = new Date(deliv_year, deliv_month -1 , deliv_date);
			var coldate = new Date(col_year, col_month -1 , col_date);
			var currentDate = new Date();
			
			console.log(delivdate);
			console.log(coldate);
			console.log(currentDate);
			
			if (delivdate < currentDate){
				var delivery_status = 'error';
				var delivery_message = '<p>Choose delivery date correcly. Delivery date must date after today !</p>';
			}
			
			if (coldate <= currentDate){
				var collection_status = 'error';
				var collection_message = '<p>Choose collection date correcly. Collection date must date  after today !</p>';
			}
			
			if(collection_status == 'error' || delivery_status == 'error'){
				$('.modal-body').html('');
				$('#binhire_date_notification_label').html('');
				$('#binhire_date_notification_label').append('Submission Error');
				
				if(delivery_message){
					$('.modal-body').append(delivery_message);
				}
				
				if(collection_message){
					$('.modal-body').append(collection_message);
				}
				
				$('#binhire_date_notification').modal('show');
				return
			}
			
			//proceed with caution
			var deliverydatesent = deliv_year+'-'+deliv_month+'-'+deliv_date;
			var collectiondatesent = col_year+'-'+col_month+'-'+col_date;
			$.ajax({
				type: "POST",
				url: theme_param.ajaxurl,
				data: {
					action : 'binhire',
					bintype : bintype,
					zipcode : zipcode,
					deliverydate: deliverydatesent,
					collectiondate: collectiondatesent,
					size : size, 
				},
				 success: function(data){
					console.log(data);
				},
				error: function( data, status, error ) { 
					console.log(data);
					console.log(status);
					console.log(error);
				}
			})
			.done(function( data ) {
				history.pushState({page: 1}, '', window.location.href);
				console.log('Successful AJAX Call! /// Return Data: ' + data);
				window.location.replace(data);
			})
			.fail(function( data ) {
				console.log('Failed AJAX Call :( /// Return Data: ' + data);
			});
		});
		
		$('#binhire_type_postcode').on('click', function(){
			var zipcode = $('[name="compare"]').val();
			$.ajax({
				type: "POST",
				url: theme_param.ajaxurl,
				data: {
					action : 'binhire',
					zipcode : zipcode,
				},
				 success: function(data){
					console.log(data);
				},
				error: function( data, status, error ) { 
					console.log(data);
					console.log(status);
					console.log(error);
				}
			})
			.done(function( data ) {
				console.log('Successful AJAX Call! /// Return Data: ' + data);
				window.location.replace(data);
			})
			.fail(function( data ) {
				console.log('Failed AJAX Call :( /// Return Data: ' + data);
			});
			
		});
		
		$('#checkout-complete').on('click', function(){
			var bestdeal = $('[name=checkout-deal]').val();
			console.log(bestdeal);
			var bintype = $('[name="checkout-bintype"]').val();
			var zipcode = $('[name="checkout-zipcode"]').val();
			var size = $('[name="checkout-binsize"]').val();
			var deliverydate = $('[name="checkout-deliverydate"]').val();
			var collectiondate = $('[name="checkout-collectiondate"]').val();
			
			$.ajax({
				type: "POST",
				url: theme_param.ajaxurl,
				data: {
					action : 'checkout',
					bestdeal : bestdeal,
					bintype : bintype,
					zipcode : zipcode,
					size : size, 
					deliverydate : deliverydate, 
					collectiondate : collectiondate
				},
				 success: function(data){
					console.log(data);
				},
				error: function( data, status, error ) { 
					console.log(data);
					console.log(status);
					console.log(error);
				}
			})
			.done(function( data ) {
				console.log('Successful AJAX Call! /// Return Data: ' + data);
				window.location.replace(data);
			})
			.fail(function( data ) {
				console.log('Failed AJAX Call :( /// Return Data: ' + data);
			});
		});
		
	});

	$(window).load(function(){
		new WOW({offset:10}).init();
		$('#loader').fadeOut(1000);
	});
	
	$(".btn").click(function(){
        $(".modal-button").modal('show');
    });
	
	$('input[type="search"]').keypress(function (e) {
		if (e.which == 13) {
			$('#search-form').submit();
			return false;    //<---- Add this line
		}
	});
})(jQuery)