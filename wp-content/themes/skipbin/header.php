<?php global $redux; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>

<?php /*<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-81271365-46"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-81271365-46');
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WHFD79M');</script>
<!-- End Google Tag Manager -->
*/ ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php echo $redux['favicon-image']['url']; ?>" type="image/vnd.microsoft.icon" />
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
	
</head>

<body <?php body_class(); ?>>
<?php /*
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WHFD79M"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="loader-wrapper">
	<div id="loader"></div>
	<div class="loader-section section-left"></div>
	<div class="loader-section section-right"></div> 
</div>

*/ ?>
<div class="top-header">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-12 col-md-12 col-lg-6  col-xl-5">
				<!--<nav class="top navbar navbar-expand-lg navbar-light">
					<button class="top navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
						<span class="fa fa-bars"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNavDropdown">
						<?php /*
							wp_nav_menu(array(
								'theme_location' => 'top-menu',
								'depth'		 => 2,
								'container'	 => false,
								'menu_class' => 'navbar-nav',
								'walker'	 => new Bootstrap_Walker_Nav_Menu(),
							));
						*/?>
					</div>
				</nav>-->
			</div>
			
			<!--<div class="col-12 col-md-9 col-lg-4 col-xl-5">-->
			<div class="col-12 d-flex justify-content-end">
				<ul class="cta-wrapper">
					<li class="cta-box">
						<a href="tel:<?php echo $redux['phone-number'];?>"><i class="fa fa-phone mr-1"></i><?php echo $redux['phone-number'];?></a>
					</li>
					<li class="cta-box">
						
						<a href="#" data-toggle="modal" data-target="#enquire-form-home"><i class="fa fa-envelope mr-1"></i> Enquire Now</a>
					</li>
				</ul>
			</div>
			
			<!-- <div class="col-12 col-md-3 col-lg-2 d-none d-md-none d-lg-block">
				<ul class="social-media">
					<li>
						<a href="<?php /* echo $redux['facebook-link']; */ ?>">
							<i class="fa fa-facebook-square mr-2"></i> <small>Facebook</small>
						</a>
					</li>  
					<?php /**
					<li>
						<a href="<?php echo $redux['twitter-link'];?>">
							<i class="fa fa-twitter-square"></i>
						</a>
					</li>
					<li>
						<a href="<?php echo $redux['google-plus-link'];?>">
							<i class="fa fa-google-plus-square"></i>
						</a>
					</li>
					
					**/ ?>
				</ul>
			</div>-->
		</div>
	</div>
</div>
<div class="navigation">
	<div class="container">
		<div class="header-wrap">
			<div class="row align-items-center">
				<div class="col-6 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<a href="<?php echo home_url()?>" class="site-logo text-left">
						<?php
							$id_image = somsweb_get_image_id($redux['logo-image']['url']);
							$image_alt = get_post_meta( $id_image, '_wp_attachment_image_alt', true);
						?>
						<img class="float-lg-left img-responsive" src="<?php echo $redux['logo-image']['url'];?>" alt="<?php echo $image_alt;?>"/>
					</a>
					
				</div>
				<div class="col-6 col-sm-6 col-md-6 d-block d-sm-block d-md-block d-lg-none text-right">
					<button class="bottom navbar-toggler mb-1" type="button" data-toggle="collapse" data-target="#navbarNavDropdownBottom" aria-controls="navbarNavDropdownBottom" aria-expanded="false" aria-label="Toggle navigation">
						<span class="fa fa-bars"></span>
					</button>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
					<nav class="navbar navbar-expand-lg navbar-light float-lg-right">
					
						<div class="collapse navbar-collapse" id="navbarNavDropdownBottom">
							<?php 
							wp_nav_menu(array(
								'theme_location' => 'main-menu',
								'depth'		 => 2,
								'container'	 => false,
								'menu_class' => 'navbar-nav',
								'walker'	 => new Bootstrap_Walker_Nav_Menu(),
							));
						?>
						</div>
					</nav>
				</div>
				
			</div>
		</div>
	</div>
</div>
<?php echo get_template_part( 'template-parts/template', 'header' ); ?>