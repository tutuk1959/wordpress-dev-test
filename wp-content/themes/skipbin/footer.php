<?php global $redux; ?>
	<footer>
		<div class="top-footer py-4">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-4">
						<div class="footer-logo">
							<div class="main-content">
								<div class="section-content">
									<!--<img src="<?php echo get_template_directory_uri();?>/assets/img/skipbin-logo_03.png" />-->
									<img src="https://somsdevone.com/wastetechnology/wp-content/uploads/2020/02/sketch-04.png" alt="">
								</div>
								<!--<div class="section-content">
									<img class="ml-lg-0"src="<?php echo get_template_directory_uri();?>/assets/img/WMAA_25.png" />
								</div>-->
								<div class="section-content d-block d-md-block d-lg-none d-flex d-md-flex">
									<ul class="social-media list-inline mx-auto justify-content-center">
										<li>
											<a href="<?php echo $redux['facebook-link'];?>">
												<i class="fa fa-facebook-square fa-2x"></i>
											</a>
										</li>
										<li>
											<a href="<?php echo $redux['twitter-link'];?>">
												<i class="fa fa-twitter-square fa-2x"></i>
											</a>
										</li>
										<li>
											<a href="<?php echo $redux['google-plus-link'];?>">
												<i class="fa fa-google-plus-square fa-2x"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-12 col-lg-4">
						<div class="footer-logo">
							<div class="main-content">
								<div class="section-content">
									<?php 
										wp_nav_menu(array(
											'theme_location' => 'footer-menu-1',
											'class' => 'header-nav',
											'container'	 => false,
										));
									?>
								</div>
							</div>
						</div>
					</div>
					
					
					<!--<div class="col-6 col-lg-2">
						<div class="footer-logo">
							<div class="main-content">
								<div class="section-content">
									<?php 
										wp_nav_menu(array(
											'theme_location' => 'footer-menu-2',
											'class' => 'header-nav',
											'container'	 => false,
										));
									?>
								</div>
							</div>
						</div>
					</div>-->
					
					<div class="col-12 col-lg-4 d-none d-md-none d-lg-block">
						<div class="footer-logo">
							<div class="main-content">
								<div class="section-content">
									<ul class="header-nav contact-info">
										<li>
											<a href="#">
												<i class="fa fa-map-marker  mr-2 text-green"></i>
												<?php echo $redux['office-address'];?>
											</a>
										</li>
										<li>
											<a href="#" data-toggle="modal" data-target="#enquire-form-home">
												<i class="fa fa-envelope  mr-2 text-green"></i>
												Enquire Now
											</a>
											
										</li>
										<li>
											<a href="tel:<?php echo $redux['phone-number'];?>">
												<i class="fa fa-phone  mr-2 text-green"></i>
												<?php echo $redux['phone-number'];?>
											</a>
										</li>
									</ul>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="bottom-footer py-3">
			<div class="container">
				<div class="main-content">
					<div class="row">
						<div class="col-12 text-center">
							<div class="section-wrapper">
								<div class="section-content text-green">
									<?php echo $redux['copyright'];?>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div class="modal fade" id="postcode_search" tabindex="-1" role="dialog" aria-labelledby="postcode_search_label" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="postcode_search_label">Postcode Search</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h5>Find your postcode</h5>
					<p>Select your state, region and suburb below</p>
					<p><strong>Step 1 </strong>Select your state:</p>
					
					<?php $result = showstate();?>
					<div id="state-placeholder">
						<div class="form-group">
							<select name="modalState" id="modalState" class="form-control">
								<option value="#" selected>Select your state</option>
								<?php foreach($result->data as $state):?>
									<option value="<?=$state->zipcode?>"><?=$state->area;?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					
					<div id="area-placeholder">
						<p><strong>Step 2 </strong>Select your region:</p>
						<span id="area-loading" class="mb-2">Loading data..</span>
						<div class="form-group" >
							<select name="modalArea" id="modalArea" class="form-control">
								
							</select>
						</div>
					</div>
					
					<div id="postcode-placeholder">
						<p><strong>Step 3 </strong> Select the postcode that best matches your area:</p>
						<span id="postcode-loading" class="mb-2">Loading data..</span>
						<div class="form-group" >
							<select name="modalPostcode" id="modalPostcode" class="form-control">
								
							</select>
						</div>
					</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="binhire_date_notification" tabindex="-1" role="dialog" aria-labelledby="binhire_date_notification_label" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header error">
					<h5 class="modal-title " id="binhire_date_notification_label"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					
				</div>
			</div>
		</div>
	</div>
	<?php echo do_shortcode('[ezyskipbin_form_popup]'); ?>
	<?php wp_footer();?>
	</body>
</html>
