<?php get_header();?>

<div id="first-section" class="features py-3">
	<div class="container">
		<div class="row d-flex justify-content-center align-items-stretch">
			<div class="col-12 col-md-12 col-lg-6 mb-3 mb-md-3 mb-lg-3 d-block d-md-block d-lg-none">
				<div class="main-content d-md-flex gray-background px-2 align-items-stretch h-100 4-easy">
					<div class="section-title align-self-center text-center">
						<h3><?php echo $redux['ezy-steps-title'];?></h3>
						<h6><?php echo $redux['ezy-steps-subtitle']?></h6>
					</div>
					<div class="section-content align-self-center">
						<img src="<?php echo $redux['ezy-steps-image']['url']?>" alt="<?php echo $redux['ezy-steps-title']?>" />
					</div>
				</div>
			</div>
			
			<div class="col-12 col-md-12 col-lg-6  mb-3 mb-md-3 mb-lg-3">
				<div class="main-content  gray-background">
					<div class="section-content">
						<a href="#" data-toggle="modal" data-target="#enquire-form-home">
							<div class="cta-main-content">
								<img src="<?php echo $redux['features-cta-image']['url'];?>" alt="Ezyskips Online Contact us via phone"/>
								<div class="cta-desc float-left text-center text-md-left">
									<h3>Enquire Now</h3>
									<?php echo $redux['features-cta-subtitle'];?>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-12 col-lg-6 mb-3 mb-md-3 mb-lg-3 d-none d-md-none d-lg-block">
				<div class="main-content d-md-flex gray-background px-2 align-items-stretch h-100 4-easy">
					<div class="section-title align-self-center text-center">
						<h3><?php echo $redux['ezy-steps-title'];?></h3>
						<h6><?php echo $redux['ezy-steps-subtitle']?></h6>
					</div>
					<div class="section-content align-self-center">
						<img src="<?php echo $redux['ezy-steps-image']['url']?>" alt="<?php echo $redux['ezy-steps-title']?>" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>