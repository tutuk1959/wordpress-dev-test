<?php
get_header(); ?>
<div style="clear:both;"></div>
<div class="page-navigation">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 col-sm-12">
				<div class="breadcrumbs">
					<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<p id="breadcrumbs">','</p>');
					}
					?>
				</div>
			</div>
			<div class="col-lg-3 col-sm-12">
				<div class="search-field">
					<?php get_search_form(); ?>
				</div>
			</div>
		</div>
	</div>
	
</div>
<div class="services-banner wow fadeInUp">
	<div class="services-container container">
		<div class="row">
			<div class="page-banner-title col-lg-12 col-sm-12 wow fadeInUp" data-wow-delay="0.1s">
				<div class="text">
					<?php if ( have_posts() ) : ?>
						<h1><?php printf( __( 'Search Results for: %s', 'cleft' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
					<?php else : ?>
						<h1 ><?php _e( 'Nothing Found', 'cleft' ); ?></h1>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="latest-archive">
		<div class="row">
			<?php if ( have_posts() ) : ?>
				<?php
				while ( have_posts() ) : the_post();?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="entry-content row wow fadeInUp" data-wow-delay="0.3s">
						<?php if(has_post_thumbnail()):?>
							<div class="custom-post">
								<div class="col-lg-4 col-sm-12">
									<?php 
										$thumb_url = get_the_post_thumbnail_url($post->ID); 
										$image_alt = get_post_meta( $thumb_url, '_wp_attachment_image_alt', true);
									?>
									<div class="single-thumbnail-wrapper">
										<img class="img-responsive" src="<?php the_post_thumbnail_url($post->ID)?>" alt="<?php echo $image_alt;?>" />
										
									</div>
								</div>
								
								<div class="col-lg-8 col-sm-12">
									<a class="title" href="<?php echo esc_url( get_permalink($post->ID) ); ?>"><?php the_title();?></a>
									<?php the_content(); ?>
									<a href="<?php echo esc_url( get_permalink($post->ID) ); ?>" class="cleft-buttons">Read More</a>
									<?php
										
					
									?>
									<div class="abstract-meta">
										<ul class="abstract-meta-details">
											<li class="date">
												<em><?php echo the_date('d-m-Y');?></em>
											</li>
											<li class="user">
												<p><i class="fa fa-users"></i><?php echo the_author();?></p>
											</li>
											<?php $term = get_term($post->ID);?>
											<?php if ($term != null):?>
											<li class="cat">
												
												<p><i class="fa fa-tags"></i><a href="<?php echo get_term_link($term->term_id);?>"></a><?php echo $term->name;?></p>
											</li>
											<?php endif;?>
										</ul>
									</div>
								</div>
							</div>
						<?php else : ?>
							<div class="col-lg-12 col-sm-12">
								<div class="custom-post">
									<a class="title" href="<?php echo esc_url( get_permalink($post->ID) ); ?>"><?php the_title();?></a>
									<?php the_content(); ?>
									<a class="read-more cleft-buttons" href="<?php echo esc_url( get_permalink($post->ID) ); ?>" class="cleft-buttons">Read More</a>
									<?php
										
									?>
									<div class="abstract-meta">
										<ul class="abstract-meta-details">
											<li class="date">
												<em><?php echo the_date('d-m-Y');?></em>
											</li>
											<li class="user">
												<p><i class="fa fa-users"></i><?php echo the_author();?></p>
											</li>
											<?php $term = get_term($post->ID);?>
											<?php if ($term != null):?>
											<li class="cat">
												
												<p><i class="fa fa-tags"></i><a href="<?php echo get_term_link($term->term_id);?>"></a><?php echo $term->name;?></p>
											</li>
											<?php endif;?>
										</ul>
									</div>
								</div>
							</div>
						<?php endif;?>
					</div><!-- .entry-content -->
				</div>
				<?php endwhile;?>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php get_footer();
