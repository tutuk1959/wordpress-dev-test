<?php global $redux; ?>
<?php if(is_home() || is_front_page()): ?>
	
	<div class="header-banner ">
		<div class="container">
			<div class="row align-items-center justify-content-center">
				<div class="col-12 col-md-12 col-lg-6">
					<div class="best-price-wrapper mb-2">
					
						<div class="price rounded-circle">
							<span><a href="#" data-toggle="popover" title="" data-content="">$</a></span>
						</div>
						<div class="best-price-container px-4">
							<div class="main-content text-left py-2">
								<h3>Today's best price</h3>
							</div>
						</div>
					</div>
					
					<div class="compare-form-wrapper">
						<div class="compare-form-container px-4">
							<div class="main-content">
								<div class="section-title text-center">
									<h3>Compare & Book Online</h3>
									<span>Instant skip hire price</span></div>
								<div class="section-content p-2">
									<form action="<?php echo esc_url(home_url('bin-hire'));?>" method="post">
										<div class="form-row d-flex align-items-center">
											<div class="col-12 col-md-8 col-lg-7 col-xl-8">
												<input type="hidden" value="home_form" name="home_form"/>
												<input type="text" class="form-control front-zipcode mb-2 mb-lg-0" name="compare" [data-home-compare] placeholder="Enter your post code here" required="true"/>
											</div>
											<div class="col-12 col-md-4 col-lg-5 col-xl-4 text-center">
												<button type="submit" name="submit" value="submit" class="button-green">GET THE BEST PRICE</button>
											</div>
										</div>
									</form>
									
								</div>
								<div class="section-footer">
									<div class="row">
										<div class="col-12 col-md-7">
											<p>Not sure of your postcode? <a href="#" data-toggle="modal" data-target="#postcode_search">Click here</a></p>
										</div>
										<div class="col-12 col-md-5 text-right">
											<ul class="cards">
												<li>
													<a href="#">
														<img class="mr-2" src="<?php echo get_template_directory_uri()?>/assets/img/visa_07.jpg" />
													</a>
												</li>
												<li>
													<a href="#">
														<img class="mr-2" src="<?php echo get_template_directory_uri()?>/assets/img/mastercard_09.jpg" />
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6">
					<div class="p-2">
						<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner" role="listbox">
								<?php $i=0;foreach ($redux['home-slider'] as $slide): ?>
									<div class="slides carousel-item <?=($i == 0) ? 'active' : ''?>">
										<img class="image-slides img-responsive d-block img-fluid" src="<?php echo $slide['image'];?>">
										<div class="carousel-caption slider-text text-center text-lg-left">
											<h3><?php echo $slide['title'];?></h3>
										</div>
									</div>
									<?php $i++;?>
								<?php endforeach;?>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
<?php endif;?>
<?php if(is_page() && !is_page('contact-us')): ?>
	<div class="content-header highlights py-3">
		<div class="content-container container" >
			<div class="row">
				<section class="col-12 page-title">
					<div class="resources-title text-center">
						<h1><?php echo get_the_title(); ?></h1>
						<span></span>
					</div>
				</section>
				<section class="col-12">
					<div class="breadcrumbs">
						<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						}
						?>
					</div>
				</section>
			</div>
		</div>
	</div>
<?php endif; ?>
		
<?php if(is_archive()  & have_posts()): ?>
	
	<div class="content-header highlights py-3">
		<div class="content-container container" >
			<div class="row">
				<section class="col-12 page-title archives-title">
					<div class="resources-title text-center">
						<?php the_archive_title( '<h1>', '</h1>' );?>
						<span></span>
					</div>
				</section>
				<section class="col-12">
					<div class="breadcrumbs">
						<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb('<p id="breadcrumbs">','</p>');
						}
						?>
					</div>
				</section>
			</div>
		</div>
	</div>
<?php endif; ?>