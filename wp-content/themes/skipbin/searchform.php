<?php
/**
 * Default Search Form
 */
?>
<form role="search" method="get" id="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div id="search-container">
		<div class="input-group">
			<div class="input-group-prepend">
				<div class="input-group-text"><i class="fa fa-search"></i></div>
			</div>
			<input type="search" class="form-control" placeholder="<?php echo esc_attr( 'Search…', 'presentation' ); ?>"  name="s" id="search-input" value="<?php echo esc_attr( get_search_query() ); ?>">
			<input style="display:none;" class="btn btn-primary" id="search-submit" value="Search" type="submit">
		</div>
	</div>
</form>