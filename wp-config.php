<?php
define('WP_CACHE', false);
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_devtest');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ct:Kkz5Ts>n7qs:C@{4@0dI~<w}TN{gN|$QJbbT-jxp<jI>g>kr?9mwc5~TcVRuY');
define('SECURE_AUTH_KEY',  '[kiY2cnX+8AE:g&GSdb{0][f$f~DOjkKjH@om!Akp(!Ziw(?Sp?d=_.kt FeU~r}');
define('LOGGED_IN_KEY',    '&:?Cox0S/pc U$tYP)&Lhe2]oLc>%T~oyQf7c~Lxk|c[5a+?bg6|GNxkaJ+#9#DQ');
define('NONCE_KEY',        '7;giea.7-/|jiRS~cC+oN7-zq/Dq;B@u9WH<:~cNud(v^_}QO2V8iDEvYRN!N!)I');
define('AUTH_SALT',        '(?jZxl_l]$3Ay>sCOf^[SXz7xT@.kN8Wo>[Ds weIYW%;9yclC>1;Xi$L(=cDA&U');
define('SECURE_AUTH_SALT', 'HmK-`35T-OA7XeGxp/ 8mJvJDPZZM c}AD>jO$exPMb1;mD3^sjv+H6{LS3P*n/u');
define('LOGGED_IN_SALT',   'LkLyT4d_64X9cK=36lMjz/c~o6Ju/Q%qt&|u&nO[>uFJ|`!<5v8Tf@BK9G#0>-7K');
define('NONCE_SALT',       ' LbXx/U-P1Fiw;}2pj$dVELhb?REx*Q`!R0>Te)Bka>p&%1n a]wNRn355xF(uBM');
define('SUPPLIER_URL', 'https://wastetechnology.ihubcrm.com/');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'skipbin_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
